﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo;

namespace Controller
{
    public class ControllerPerfiles
    {
        
        public ControllerPerfiles()
        {
            ObtDatos = new DataModel();            
        }

        public DataModel ObtDatos { get; set; }

        public DataModel CalcularFibonacci()
        {
            ObtDatos.FibonacciValue = 1;
            int aux = 0;
            int sum = 1;
            for (int i = 1; i < ObtDatos.Index; i++)
            {                
                ObtDatos.FibonacciValue = sum + aux;
                aux = sum;
                sum = ObtDatos.FibonacciValue;                                
            }
            return ObtDatos;
        }
    }
}
