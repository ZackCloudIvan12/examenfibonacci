﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Modelo
{
    public class DataModel : INotifyPropertyChanged
    {
        private int index;
        private int fibonacciValue;
        
        public DataModel()
        {
            Index = 0;
            FibonacciValue = 1;
        }

        public int Index
        {
            get { return index; }
            set { index = value;
            notify("Index");
            }
        }
        public int FibonacciValue
        {
            get { return fibonacciValue; }
            set { fibonacciValue = value;
            notify("FibonacciValue");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void notify(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
   
    