﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Controller.ControllerPerfiles control_Inf;
        public MainWindow()
        {
            InitializeComponent();
            control_Inf = new Controller.ControllerPerfiles();
            this.Datos.DataContext = control_Inf.ObtDatos;
        }

        private void mostrar_Click(object sender, RoutedEventArgs e)
        {            
            this.Datos.DataContext = control_Inf.CalcularFibonacci();
        }


    }
}
